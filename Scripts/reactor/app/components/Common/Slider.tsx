﻿import * as React from 'react';
import { Container } from 'flux/utils';
import Slider from "react-slick";
import * as DocumentActions from '../../actions/DocumentActions';
import MainImage = require("./MainImage");
import Store = require('../../stores/DocumentViewStore');
import * as Moment from 'moment';
interface SliderProps extends React.Props<TheSlider> {
       
    }

interface SliderState {
    images: DocumentActions.ImgLocAction[]
    }

 class TheSlider extends React.Component<SliderProps, SliderState> {
       
    constructor(props: SliderProps) {
            super(props);

            
     }
     static getStores() {
         return [Store];
     }
     static calculateState(prevState: SliderState, nextProps: SliderProps): SliderState {
       
         return {
             images: Store.getImages()
         }

     }
    render() {
        let images = [];
        for (var i = 0; i < this.state.images.length; i++) {
            images.push(React.createElement(MainImage, { url: this.state.images[i].url, copyright: '', title: this.state.images[i].title, description: this.state.images[i].description, isThumbnail: true }));
        }
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1
        };
        return (<div><Slider {...settings}>
            {  
                images
            }
        </Slider></div>)
        }
        componentDidMount() {
            DocumentActions.GetDatedImages(5);
    }
}

export = Container.create(TheSlider, { withProps: true }) as React.ComponentClass<SliderProps>;
