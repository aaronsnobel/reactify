﻿import * as React from 'react';
import { Container } from 'flux/utils';
import * as DocumentActions from '../../actions/DocumentActions';
import Store = require('../../stores/DocumentViewStore');
import { ComponentClass } from 'react';
import * as moment from 'moment';
    interface MainImageProps extends React.Props<MainImage> {
        url: string,
        copyright: string,
        title: string,
        description: string,
        isThumbnail: boolean
    }

    interface MainImageState {
        url: string,
        copyright: string,
        title: string,
        description: string
    }

    class MainImage extends React.Component<MainImageProps, MainImageState> {
       
        constructor(props: MainImageProps) {
            super(props);

            this.state = {
                url: props.url,
                copyright: props.copyright,
                title: props.title,
                description: props.description
            }
        }
        static getStores() {
            return [Store];
        }
        static calculateState(prevState: MainImageState, nextProps: MainImageProps): MainImageState {
            if (!nextProps.isThumbnail) {
                let mainImage: DocumentActions.IOTDAction = Store.getMainImage();
                return {
                    url: mainImage.url,
                    copyright: mainImage.copyright,
                    title: mainImage.title,
                    description: mainImage.description
                }
            }
            
        }

       
        render() {
            return (<div>
                <div className='mi_container'>
                    <div className='mi_title'>{this.state.title}</div>
                    <div className='mi_center'>
                        <div className='mi_description'>{this.state.description}</div>
                        <div className='mi_img'><img src={this.state.url} />
                            {
                                this.state.copyright &&
                                    <span className="mi_copyright">&copy; Copyright {this.state.copyright}</span>
                            }
                        </div>
                        
                    </div>
                    
                </div>





            </div>)
        }
        componentDidMount() {
            if (!this.props.isThumbnail)
                DocumentActions.GetIOTD(moment().format("YYYY-MM-DD"));
        }
    }


export = Container.create(MainImage, { withProps: true }) as React.ComponentClass<MainImageProps>;
