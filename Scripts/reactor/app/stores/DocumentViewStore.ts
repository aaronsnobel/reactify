﻿import { Store } from 'flux/utils';
import Dispatcher = require('../dispatcher/dispatcher');


import * as DocumentActions from '../actions/DocumentActions';
import { IOTDAction } from '../actions/DocumentActions';


class InternalState {
    mainImage: DocumentActions.IOTDAction;
    Images: DocumentActions.ImgLocAction[];
    

    constructor() {
        this.mainImage = { url: '', copyright: '', description: '', title: '' };
        this.Images = [];
    }
}
class DocumentViewStore extends Store<any> {
    private oInternalState: InternalState;
   
    

    constructor(dispatcher: typeof Dispatcher) {
        super(dispatcher);

        this._initializeState();
    }
    

    getMainImage(): DocumentActions.IOTDAction {
        return this.oInternalState.mainImage;
    }
    getImages(): DocumentActions.ImgLocAction[] {
        return this.oInternalState.Images;
    }

    private _initializeState() {
        this.oInternalState = new InternalState();
        
    }
    

    __onDispatch(action: any) {
        if (action instanceof DocumentActions.IOTDAction) {
            let typedAction = action as DocumentActions.IOTDAction;
            this.oInternalState.mainImage = typedAction;

            this.__emitChange();
        }
        if (action instanceof DocumentActions.ImgLocAction) {
            let typedAction = action as DocumentActions.ImgLocAction;
            this.oInternalState.Images.push(typedAction);

            this.__emitChange();
        }
      
    }
}

export = new DocumentViewStore(Dispatcher);
