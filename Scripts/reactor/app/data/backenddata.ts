﻿import * as DocumentActions from '../actions/DocumentActions';





export function GetImageOfTheDay(aDate: string): void {
   
    fetch(`https://api.nasa.gov/planetary/apod?api_key=twxA69OqDPB3xl6B9585PIyI1FWtk4l4syz3byPO&date=${aDate}`)
        .then((response:any) => (response.json()))
        .then((responseJson:any) => {
                DocumentActions.ReceiveIOTD(responseJson);
            });
            
}
export function GetLocationImage(sDate: string, sLat: string, sLong: string): void {
    fetch(`https://api.nasa.gov/planetary/earth/imagery?api_key=twxA69OqDPB3xl6B9585PIyI1FWtk4l4syz3byPO&date=${sDate}&lat=${sLat}&lon=${sLong}`)
        .then((response: any) => (response.json()))
        .then((responseJson: any) => {
            DocumentActions.ReceiveLocImg(responseJson);
        });

}

