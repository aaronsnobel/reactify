﻿import React = require('react');
import * as ReactDOM from 'react-dom';
import MainImage = require('./components/Common/MainImage');
import TheSlider = require("./components/Common/Slider");

function deferAll(obj:any):any {
    return _.mapObject(obj, function (func) {
        return function () {
            var context: any = this;
            var args: IArguments = arguments;
            _.defer(() => {
                func.apply(context, args);
            });
        }
    });
} 
ReactDOM.render(React.createElement(MainImage), document.getElementById('app'));
ReactDOM.render(React.createElement(TheSlider), document.getElementById('slider'));
export = {
    React: React,
    ReactDom: ReactDOM,
    
    Dispatcher: require('./dispatcher/dispatcher'),
    DeferedActions: {
    },
    Stores: {
       
        DocumentViewStore: require('./stores/DocumentViewStore')
       
    },
    Helpers: {
        
    },
    Tiles: {
        
    },
    Components: {
        
    }
};
