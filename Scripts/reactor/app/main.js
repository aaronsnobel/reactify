"use strict";
var React = require("react");
var ReactDom = require("react-dom");
var ExternalActions = require("./actions/ExternalActions");
var BreadcrumbBar = require("./components/Breadcrumbs/BreadcrumbBar");
var SharingDialog = require("./components/common/SharingDialog");
var CommentSetPicker = require("./components/DocumentView/CommentSetPicker");
var BatchSetPicker = require("./components/Dashboard/Tiles/BatchSetPicker");
var LabelManagerDialog_1 = require("./components/common/LabelManagerDialog");
var FieldSelectionControl_1 = require("./components/Common/FieldSelectionControl");
var PrivateHelpTip_1 = require("./components/Common/PrivateHelpTip");
var KendoSelections = require("./components/Common/KendoUsersAndGroupsSelections");
$.fn.PrivateHelpTip = PrivateHelpTip_1.PrivateHelpTip;
$.fn.KendoMultiSelectUsersAndGroups = KendoSelections.KendoMultiSelectUsersAndGroups;
$.fn.KendoMultiSelectUsers = KendoSelections.KendoMultiSelectUsers;
$.fn.KendoMultiSelectGroups = KendoSelections.KendoMultiSelectGroups;
$.fn.KendoDropDownListUsersAndGroups = KendoSelections.KendoDropDownListUsersAndGroups;
$.fn.KendoDropDownListUsers = KendoSelections.KendoDropDownListUsers;
$.fn.KendoDropDownListGroups = KendoSelections.KendoDropDownListGroups;
var Toggle_1 = require("office-ui-fabric-react/lib/Toggle");
ExternalActions.setDeviceInfo(gDeviceModeController.bBypassMobile, gDeviceModeController.bHandheldDevice, gDeviceModeController.iDeviceType, gDeviceModeController.eCurrentDeviceMode, $.client.browser == geBrowser.IE, $.client.os == geOS.IOS, $.client.browser != geBrowser.CHROME && $.client.browser != geBrowser.FIREFOX, $.client.browser == geBrowser.FIREFOX);
ReactDom.render(React.createElement(BreadcrumbBar), document.getElementById('breadcrumbs'));
// The CommentSetPicker on the Comments Tile is tied to document view
ReactDom.render(React.createElement(CommentSetPicker, { traincar: XeraTraincar.DOCUMENT_VIEW }), document.getElementById('comments-tile-set-selector'));
// The BatchSetPicker on the Batch Manager Tile
ReactDom.render(React.createElement(BatchSetPicker), document.getElementById('batch-manager-set-selector'));
function deferAll(obj) {
    return _.mapObject(obj, function (func) {
        return function () {
            var context = this;
            var args = arguments;
            _.defer(function () {
                func.apply(context, args);
            });
        };
    });
}
module.exports = {
    React: React,
    ReactDom: ReactDom,
    SharingDialog: SharingDialog.sharingDialog,
    Dispatcher: require('./dispatcher/dispatcher'),
    DocumentActions: require('./actions/DocumentActions'),
    ExternalActions: require('./actions/ExternalActions'),
    ImageActions: require('./actions/ImageActions'),
    WordMarkingActions: require('./actions/WordMarkingActions'),
    TextViewActions: require('./actions/TextViewActions'),
    GridActions: require('./actions/GridActions'),
    NativeActions: require('./actions/NativeActions'),
    UserSessionActions: require('./actions/UserSessionActions'),
    ActionLoggingActions: require('./actions/ActionLoggingActions'),
    WorkProductActions: require('./actions/WorkProductActions'),
    SystemActions: require('./actions/SystemActions'),
    DeferedActions: {
        DocumentActions: deferAll(require('./actions/DocumentActions')),
        ExternalActions: deferAll(require('./actions/ExternalActions')),
        ImageActions: deferAll(require('./actions/ImageActions')),
        WordMarkingActions: deferAll(require('./actions/WordMarkingActions')),
        TextViewActions: deferAll(require('./actions/TextViewActions')),
        GridActions: deferAll(require('./actions/GridActions')),
        NativeActions: deferAll(require('./actions/NativeActions')),
        UserSessionActions: deferAll(require('./actions/UserSessionActions')),
        ActionLoggingActions: deferAll(require('./actions/ActionLoggingActions')),
        WorkProductActions: deferAll(require('./actions/WorkProductActions')),
        SystemActions: deferAll(require('./actions/SystemActions'))
    },
    Stores: {
        ConsolidatedViewerStore: require('./stores/ConsolidatedViewerStore'),
        DatabaseInformationStore: require('./stores/DatabaseInformationStore'),
        DocumentViewStore: require('./stores/DocumentViewStore'),
        SelectedRecordStore: require('./stores/SelectedRecordStore'),
        TextViewStore: require('./stores/TextViewStore'),
        BrowserInfoStore: require('./stores/BrowserInfoStore'),
        WordMarkingStore: require('./stores/WordMarkingStore'),
        BreadcrumbStore: require('./stores/BreadcrumbStore'),
        UserSessionStore: require('./stores/UserSessionStore'),
        ActionLoggingStore: require('./stores/ActionLoggingStore'),
        CentreAreaStore: require('./stores/CentreAreaStore'),
        WorkProductStore: require('./stores/WorkProductStore'),
        SystemStore: require('./stores/SystemStore'),
        UserStore: require('./stores/UserStore'),
        DataProcessingTileStore: require('./stores/DataProcessingTileStore')
    },
    Helpers: {
        Enums: require('./helpers/SharedEnums'),
        String: require('./helpers/ProcessingHelper').StringHelper,
        Array: require('./helpers/ProcessingHelper').ArrayHelper,
        Sort: require('./helpers/ProcessingHelper').SortHelper,
        TableView: require('./helpers/TableViewHelper'),
        PermissionGridHelper: require('./helpers/PermissionGridHelper'),
        Features: require('./helpers/classes/FeaturesHelper').FeaturesHelper,
        WPCDialog: require('./components/Common/WorkProductContainerDialog').WPCDialog,
        Job: require('./helpers/ProcessingHelper').JobHelper,
        Kendo: require('./helpers/ProcessingHelper').KendoHelper,
        Download: require('./helpers/ProcessingHelper').DownloadHelper
    },
    Tiles: {
        SearchTile: require('./components/Dashboard/Tiles/SearchTiles').SearchTile,
        DocumentRulesTile: require('./components/Dashboard/Tiles/DocumentRules').DocumentRulesTile,
        QuickSearchTile: require('./components/Dashboard/Tiles/QuickSearch').QuickSearchTile,
        MyProjectsTile: require('./components/Dashboard/Tiles/MyProjectsTile').MyProjectsTile,
        DataProcessingUITile: require('./components/Dashboard/Tiles/DataProcessingTile').DataProcessingUITile
    },
    Components: {
        BatchSetPicker: BatchSetPicker,
        Toggle: Toggle_1.Toggle,
        LabelManagerDialog: LabelManagerDialog_1.labelManagerDialog,
        FieldSelectionControl: FieldSelectionControl_1.FieldSelectionControl
    }
};
