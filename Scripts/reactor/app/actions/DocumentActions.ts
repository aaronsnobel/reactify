﻿import * as Dispatcher from '../dispatcher/dispatcher';
import * as BackendData from '../data/backenddata';
import * as moment from 'moment';
require("twix");
import BaseActions = require('./BaseActions');

export class IOTDAction {// extends BaseActions.TraincarAction {
    url: string;
    title: string;
    description: string;
    copyright: string;
    constructor(obj: IOTDAction) {
        //super(obj);
        this.url = obj.url;
        this.title = obj.title;
        this.description = obj.description;
        this.copyright = obj.copyright;
       
    }
}
export class ImgLocAction {// extends BaseActions.TraincarAction {
    url: string;
    title: string;
    description: string;
   
    constructor(obj: ImgLocAction) {
        //super(obj);
        this.url = obj.url;
        this.title = obj.title;
        this.description = obj.description;
        

    }
}
export function recordSelected() {
   
   // Dispatcher.dispatch(new SelectedAction({}));

   
}
export function GetDatedImages(daysFromTo: number) {
    let lan: string = "";
    let lon: string = "";
    let startDate = moment().subtract(daysFromTo, "d");
    let endDate = moment().add(daysFromTo, "d");
    let iter = moment(startDate).twix(endDate).iterate("days");
    while (iter.hasNext()) {
        BackendData.GetLocationImage(iter.next().format("YYYY-MM-DD"), "-81.255358", "42.97835");

    }
    
}
export function GetIOTD(sDate?: string) {
    BackendData.GetImageOfTheDay(sDate ? sDate : "2018-01-01");
}
export function ReceiveLocImg(jsonResponse: any) {
    Dispatcher.dispatch(new ImgLocAction ({
        url: jsonResponse.url,
        title: jsonResponse.date,
        description: jsonResponse.resource.dataset
        
    }));

}
export function ReceiveIOTD(jsonResponse: any) {
    Dispatcher.dispatch(new IOTDAction({
        url: jsonResponse.url,
        title: jsonResponse.title,
        description: jsonResponse.explanation,
       copyright: jsonResponse.copyright
    }));
}

