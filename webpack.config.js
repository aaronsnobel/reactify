/// <binding ProjectOpened='Watch - Development' />
var path = require('path');
var webpack = require('webpack');
var ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

var release = process.argv.indexOf('-p') !== -1;

var aPlugins = [
    new ForkTsCheckerWebpackPlugin({ tsconfig: path.resolve(__dirname, 'tsconfig.json') }),
    new webpack.optimize.CommonsChunkPlugin({ minChunks: 2, name: 'Common' })
];

if (release) {
    // push this in to make the react stuff get minified nicely
    aPlugins.push(new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"production"' }));
    aPlugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
    entry: {
        Main: [path.resolve(__dirname, 'Scripts/reactor/app/main.ts')]
        
    },
    output: {
        path: path.resolve(__dirname, 'Scripts/reactor/built'),
        filename: release ? '[name].min.js' : '[name].js',
        publicPath: '/Scripts/reactor/built/',
        library: 'gReactor[name]'
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    module: {
        rules: [{
            test: /\.ts(x?)$/,
            use: [{
                loader: 'ts-loader',
                options: {
                    transpileOnly: true // fork-ts-checker plugin will handle type checking
                }
            }]
        }]
    },
    plugins: aPlugins,
    devtool: 'source-map'
};
